// import * as React from "react";
// import { Icon } from "native-base";
// import { AppColorPrimary } from "@taxi/styles/theme";
// import { CusomProps } from "./props";
// import { IconTypes } from "@taxi/interfaces/icon-type";

// export class TaxiAppIcon extends React.Component<CusomProps> {
//   state = {};
//   render() {
//     const {
//       color = AppColorPrimary.C700,
//       fontSize = 20,
//       fontFamily = IconTypes.MaterialIcons,
//       style = {},
//       name
//     } = this.props;
//     return (
//       <Icon
//         style={{ color: color, ...style, fontSize: fontSize }}
//         name={name}
//         type={fontFamily}
//       />
//     );
//   }
// }
