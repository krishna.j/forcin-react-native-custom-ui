
import { View, TouchableOpacity } from "react-native";
import { Icon } from "native-base";

import React, { useState } from "react";

export interface CustomProps {
    containerStyle: any
    iconThumbsUpColor: any
    iconThumbsUpHoverColor: any
    iconThumbsDownColor: any
    iconThumbsDownHoverColor: any


}


export const ThumbsUpRating = (initialValue: any, validators: Array<any>) => {
    const [isThumbsUpHover, setIsThumbsUpHover] = useState(false)
    const [isThumbsDownHover, setIsThumbsDownHover] = useState(false)
    const [value, setValue] = useState(initialValue);
    const [errorMesage, setErrorMesage] = useState("");
    const [isValid, setIsValid] = useState(false);

    const OnPress = (value:boolean) => {
        setValue(value)
    }
    const widget = (props:CustomProps) => {
        return <View style={{ flexDirection: "row", }}>
            <TouchableOpacity onPress={() => { OnPress(true) }}  style={{ flex: .5, alignItems: "center" }}>
                <Icon color={ ( !value == true) ? props.iconThumbsUpColor : props.iconThumbsUpHoverColor } type="MaterialIcons" name="thumb-up" style={{ fontSize: 50}}></Icon>

            </TouchableOpacity>

            <TouchableOpacity onPress={() => OnPress(false)}  style={{ flex: .5, alignItems: "center" }}>
                <Icon color={( value == true)?props.iconThumbsDownColor:props.iconThumbsDownHoverColor} type="MaterialIcons" name="thumb-down" style={{ fontSize: 50}}></Icon>

            </TouchableOpacity>
        </View>
    }
    return {
        value,
        setValue,

        widget,
        valid: isValid
    };
}