export const isRequired = value => {
    if (value.length > 2) {
        return true;
    }
    return false;
};

export const requiredValidator = (message: string) => {
    const validate = (value) => {

        if (!isRequired(value)) {
            return { status: false, message: message }

        }
        return { status: true, message: "" }
    }
    return { validate }

};

export const validators = {
    requiredValidator
};

export const validate = (validators: Array<any>, value: any) => {
    let isValid = true;
    let message = ""
    for (var item of validators) {
        let validator = item.validate(value);
        isValid = validator.status
        message = validator.message
        if (!isValid) break;
    }
    return { isValid, message };
};
