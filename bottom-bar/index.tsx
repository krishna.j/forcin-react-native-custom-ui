import React, { useEffect, useState } from "react";
import { View, Dimensions ,Animated} from "react-native";
import { styles } from "./style";
import { CustomProps } from "./props";
// import AnimatedRegion from "../Interactable";
import Interactable from 'react-native-interactable';

export const Screen = {
  width: Dimensions.get("window").width,
  height: Dimensions.get("window").height - 75
};
// import { TaxiAppIcon } from "../icon";
// import { IconTypes } from "@ueTrack/core/interfaces/icon-type"

export function BottomContainer(props: CustomProps) {
  const [deltaY] = useState(new Animated.Value(Screen.height / 2));
  const [showDownArrow, setShowDownArrow] = useState(false);
  useEffect(() => {
    deltaY.addListener(({ value }) => {
      if (value < props.outputRangeFrom / 2 + 100) {
        setShowDownArrow(true);
      } else {
        setShowDownArrow(false);
      }
      if (props.onAnimateValueChange) {
        props.onAnimateValueChange(value);
      }
    });
    return () => {
      deltaY.removeAllListeners();
    };
  });

  const {
    outputRangeFrom = 0,
    outputRangeTo = 0,
    initialPosition = 0,
    customStyle = {},
    showIcon = true,
    dragEnabled=false
  } = props;
  return (
    <Animated.View
      pointerEvents={"box-none"}
      style={[
        styles.panelContainer,


        {
          position: "absolute",
          top: 0,
          backgroundColor: "transparent",
          opacity: deltaY.interpolate({
            inputRange: [0, Screen.height],
            outputRange: [outputRangeFrom, outputRangeTo],
            extrapolateRight: "clamp"
          })
        },
        customStyle,
      ]}
    >
      <Interactable.View
        verticalOnly={true}
        snapPoints={[{ y: outputRangeFrom }, { y: outputRangeTo }]}
        // ref={() => {}}
        // boundaries={{ top: outputRangeTo }}
        initialPosition={{ y: initialPosition }}
        dragEnabled={props.dragEnabled}
        animatedValueY={deltaY}
      >
        <View style={{ borderTopRightRadius: 30, borderTopLeftRadius: 30, backgroundColor: props.backgroundColor, height: "100%" }}>
          {/* {showIcon && (
            // <TaxiAppIcon
            //   name={showDownArrow ? "chevron-down" : "chevron-up"}
            //   style={{ textAlign: "center" }}
            //   fontSize={30}
            //   fontFamily={IconTypes.FontAwesome5}
            // />
          )} */}
          {props.children}
        </View>
      </Interactable.View>
    </Animated.View>
  );
}
